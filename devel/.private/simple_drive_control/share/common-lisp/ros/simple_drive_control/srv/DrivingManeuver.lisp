; Auto-generated. Do not edit!


(cl:in-package simple_drive_control-srv)


;//! \htmlinclude DrivingManeuver-request.msg.html

(cl:defclass <DrivingManeuver-request> (roslisp-msg-protocol:ros-message)
  ((direction
    :reader direction
    :initarg :direction
    :type cl:string
    :initform "")
   (steering
    :reader steering
    :initarg :steering
    :type cl:string
    :initform "")
   (distance
    :reader distance
    :initarg :distance
    :type cl:float
    :initform 0.0))
)

(cl:defclass DrivingManeuver-request (<DrivingManeuver-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <DrivingManeuver-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'DrivingManeuver-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name simple_drive_control-srv:<DrivingManeuver-request> is deprecated: use simple_drive_control-srv:DrivingManeuver-request instead.")))

(cl:ensure-generic-function 'direction-val :lambda-list '(m))
(cl:defmethod direction-val ((m <DrivingManeuver-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_drive_control-srv:direction-val is deprecated.  Use simple_drive_control-srv:direction instead.")
  (direction m))

(cl:ensure-generic-function 'steering-val :lambda-list '(m))
(cl:defmethod steering-val ((m <DrivingManeuver-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_drive_control-srv:steering-val is deprecated.  Use simple_drive_control-srv:steering instead.")
  (steering m))

(cl:ensure-generic-function 'distance-val :lambda-list '(m))
(cl:defmethod distance-val ((m <DrivingManeuver-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_drive_control-srv:distance-val is deprecated.  Use simple_drive_control-srv:distance instead.")
  (distance m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <DrivingManeuver-request>) ostream)
  "Serializes a message object of type '<DrivingManeuver-request>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'direction))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'direction))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'steering))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'steering))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'distance))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <DrivingManeuver-request>) istream)
  "Deserializes a message object of type '<DrivingManeuver-request>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'direction) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'direction) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'steering) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'steering) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'distance) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<DrivingManeuver-request>)))
  "Returns string type for a service object of type '<DrivingManeuver-request>"
  "simple_drive_control/DrivingManeuverRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'DrivingManeuver-request)))
  "Returns string type for a service object of type 'DrivingManeuver-request"
  "simple_drive_control/DrivingManeuverRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<DrivingManeuver-request>)))
  "Returns md5sum for a message object of type '<DrivingManeuver-request>"
  "1846ad2c403ea95a579ef966fc52c809")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'DrivingManeuver-request)))
  "Returns md5sum for a message object of type 'DrivingManeuver-request"
  "1846ad2c403ea95a579ef966fc52c809")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<DrivingManeuver-request>)))
  "Returns full string definition for message of type '<DrivingManeuver-request>"
  (cl:format cl:nil "string direction~%string steering~%float32 distance~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'DrivingManeuver-request)))
  "Returns full string definition for message of type 'DrivingManeuver-request"
  (cl:format cl:nil "string direction~%string steering~%float32 distance~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <DrivingManeuver-request>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'direction))
     4 (cl:length (cl:slot-value msg 'steering))
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <DrivingManeuver-request>))
  "Converts a ROS message object to a list"
  (cl:list 'DrivingManeuver-request
    (cl:cons ':direction (direction msg))
    (cl:cons ':steering (steering msg))
    (cl:cons ':distance (distance msg))
))
;//! \htmlinclude DrivingManeuver-response.msg.html

(cl:defclass <DrivingManeuver-response> (roslisp-msg-protocol:ros-message)
  ((success
    :reader success
    :initarg :success
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass DrivingManeuver-response (<DrivingManeuver-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <DrivingManeuver-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'DrivingManeuver-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name simple_drive_control-srv:<DrivingManeuver-response> is deprecated: use simple_drive_control-srv:DrivingManeuver-response instead.")))

(cl:ensure-generic-function 'success-val :lambda-list '(m))
(cl:defmethod success-val ((m <DrivingManeuver-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader simple_drive_control-srv:success-val is deprecated.  Use simple_drive_control-srv:success instead.")
  (success m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <DrivingManeuver-response>) ostream)
  "Serializes a message object of type '<DrivingManeuver-response>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'success) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <DrivingManeuver-response>) istream)
  "Deserializes a message object of type '<DrivingManeuver-response>"
    (cl:setf (cl:slot-value msg 'success) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<DrivingManeuver-response>)))
  "Returns string type for a service object of type '<DrivingManeuver-response>"
  "simple_drive_control/DrivingManeuverResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'DrivingManeuver-response)))
  "Returns string type for a service object of type 'DrivingManeuver-response"
  "simple_drive_control/DrivingManeuverResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<DrivingManeuver-response>)))
  "Returns md5sum for a message object of type '<DrivingManeuver-response>"
  "1846ad2c403ea95a579ef966fc52c809")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'DrivingManeuver-response)))
  "Returns md5sum for a message object of type 'DrivingManeuver-response"
  "1846ad2c403ea95a579ef966fc52c809")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<DrivingManeuver-response>)))
  "Returns full string definition for message of type '<DrivingManeuver-response>"
  (cl:format cl:nil "bool success~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'DrivingManeuver-response)))
  "Returns full string definition for message of type 'DrivingManeuver-response"
  (cl:format cl:nil "bool success~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <DrivingManeuver-response>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <DrivingManeuver-response>))
  "Converts a ROS message object to a list"
  (cl:list 'DrivingManeuver-response
    (cl:cons ':success (success msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'DrivingManeuver)))
  'DrivingManeuver-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'DrivingManeuver)))
  'DrivingManeuver-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'DrivingManeuver)))
  "Returns string type for a service object of type '<DrivingManeuver>"
  "simple_drive_control/DrivingManeuver")