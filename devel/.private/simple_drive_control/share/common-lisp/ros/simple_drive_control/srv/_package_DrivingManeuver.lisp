(cl:in-package simple_drive_control-srv)
(cl:export '(DIRECTION-VAL
          DIRECTION
          STEERING-VAL
          STEERING
          DISTANCE-VAL
          DISTANCE
          SUCCESS-VAL
          SUCCESS
))