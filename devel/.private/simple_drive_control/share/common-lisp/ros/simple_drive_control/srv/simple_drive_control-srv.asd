
(cl:in-package :asdf)

(defsystem "simple_drive_control-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "DrivingManeuver" :depends-on ("_package_DrivingManeuver"))
    (:file "_package_DrivingManeuver" :depends-on ("_package"))
  ))