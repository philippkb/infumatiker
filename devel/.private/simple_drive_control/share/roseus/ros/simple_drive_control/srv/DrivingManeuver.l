;; Auto-generated. Do not edit!


(when (boundp 'simple_drive_control::DrivingManeuver)
  (if (not (find-package "SIMPLE_DRIVE_CONTROL"))
    (make-package "SIMPLE_DRIVE_CONTROL"))
  (shadow 'DrivingManeuver (find-package "SIMPLE_DRIVE_CONTROL")))
(unless (find-package "SIMPLE_DRIVE_CONTROL::DRIVINGMANEUVER")
  (make-package "SIMPLE_DRIVE_CONTROL::DRIVINGMANEUVER"))
(unless (find-package "SIMPLE_DRIVE_CONTROL::DRIVINGMANEUVERREQUEST")
  (make-package "SIMPLE_DRIVE_CONTROL::DRIVINGMANEUVERREQUEST"))
(unless (find-package "SIMPLE_DRIVE_CONTROL::DRIVINGMANEUVERRESPONSE")
  (make-package "SIMPLE_DRIVE_CONTROL::DRIVINGMANEUVERRESPONSE"))

(in-package "ROS")





(defclass simple_drive_control::DrivingManeuverRequest
  :super ros::object
  :slots (_direction _steering _distance ))

(defmethod simple_drive_control::DrivingManeuverRequest
  (:init
   (&key
    ((:direction __direction) "")
    ((:steering __steering) "")
    ((:distance __distance) 0.0)
    )
   (send-super :init)
   (setq _direction (string __direction))
   (setq _steering (string __steering))
   (setq _distance (float __distance))
   self)
  (:direction
   (&optional __direction)
   (if __direction (setq _direction __direction)) _direction)
  (:steering
   (&optional __steering)
   (if __steering (setq _steering __steering)) _steering)
  (:distance
   (&optional __distance)
   (if __distance (setq _distance __distance)) _distance)
  (:serialization-length
   ()
   (+
    ;; string _direction
    4 (length _direction)
    ;; string _steering
    4 (length _steering)
    ;; float32 _distance
    4
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _direction
       (write-long (length _direction) s) (princ _direction s)
     ;; string _steering
       (write-long (length _steering) s) (princ _steering s)
     ;; float32 _distance
       (sys::poke _distance (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _direction
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _direction (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _steering
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _steering (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float32 _distance
     (setq _distance (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;;
   self)
  )

(defclass simple_drive_control::DrivingManeuverResponse
  :super ros::object
  :slots (_success ))

(defmethod simple_drive_control::DrivingManeuverResponse
  (:init
   (&key
    ((:success __success) nil)
    )
   (send-super :init)
   (setq _success __success)
   self)
  (:success
   (&optional (__success :null))
   (if (not (eq __success :null)) (setq _success __success)) _success)
  (:serialization-length
   ()
   (+
    ;; bool _success
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _success
       (if _success (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _success
     (setq _success (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass simple_drive_control::DrivingManeuver
  :super ros::object
  :slots ())

(setf (get simple_drive_control::DrivingManeuver :md5sum-) "1846ad2c403ea95a579ef966fc52c809")
(setf (get simple_drive_control::DrivingManeuver :datatype-) "simple_drive_control/DrivingManeuver")
(setf (get simple_drive_control::DrivingManeuver :request) simple_drive_control::DrivingManeuverRequest)
(setf (get simple_drive_control::DrivingManeuver :response) simple_drive_control::DrivingManeuverResponse)

(defmethod simple_drive_control::DrivingManeuverRequest
  (:response () (instance simple_drive_control::DrivingManeuverResponse :init)))

(setf (get simple_drive_control::DrivingManeuverRequest :md5sum-) "1846ad2c403ea95a579ef966fc52c809")
(setf (get simple_drive_control::DrivingManeuverRequest :datatype-) "simple_drive_control/DrivingManeuverRequest")
(setf (get simple_drive_control::DrivingManeuverRequest :definition-)
      "string direction
string steering
float32 distance
---
bool success
")

(setf (get simple_drive_control::DrivingManeuverResponse :md5sum-) "1846ad2c403ea95a579ef966fc52c809")
(setf (get simple_drive_control::DrivingManeuverResponse :datatype-) "simple_drive_control/DrivingManeuverResponse")
(setf (get simple_drive_control::DrivingManeuverResponse :definition-)
      "string direction
string steering
float32 distance
---
bool success
")



(provide :simple_drive_control/DrivingManeuver "1846ad2c403ea95a579ef966fc52c809")


