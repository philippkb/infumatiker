#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/kaspar/robotics/catkin_ws_InFUmatiker/devel/.private/simple_drive_control:$CMAKE_PREFIX_PATH"
export PWD='/home/kaspar/robotics/catkin_ws_InFUmatiker/build/simple_drive_control'
export ROSLISP_PACKAGE_DIRECTORIES="/home/kaspar/robotics/catkin_ws_InFUmatiker/devel/.private/simple_drive_control/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/kaspar/robotics/catkin_ws_InFUmatiker/src/simple_drive_control:$ROS_PACKAGE_PATH"