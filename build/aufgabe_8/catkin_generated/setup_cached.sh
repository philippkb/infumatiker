#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/kaspar/robotics/catkin_ws_InFUmatiker/devel/.private/aufgabe_8:$CMAKE_PREFIX_PATH"
export PWD='/home/kaspar/robotics/catkin_ws_InFUmatiker/build/aufgabe_8'
export ROSLISP_PACKAGE_DIRECTORIES="/home/kaspar/robotics/catkin_ws_InFUmatiker/devel/.private/aufgabe_8/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/kaspar/robotics/catkin_ws_InFUmatiker/src/aufgabe_8:$ROS_PACKAGE_PATH"