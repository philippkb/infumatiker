#!/usr/bin/env python3
import rospy
from simple_parking_maneuver.srv import *
from simple_drive_control.srv import *


class SimpleParkingManeuver:

    def __init__(self):
        rospy.init_node("simple_parking_maneuver")

        self.driving_maneuver_client = rospy.ServiceProxy("driving_maneuver", DrivingManeuver)
        self.parking_service = rospy.Service("parking_maneuver", ParkingManeuver, self.parking_maneuver)

    def parking_maneuver(self, request):
        rospy.loginfo(rospy.get_caller_id() + ": callbackBackwardLongitudinal, direction = " + request.direction)
        if request.direction == "forwardStraight":
            self.driving_maneuver_client.call(direction="forward", steering="straight", distance=300)
        elif request.direction == "forwardLeft":
            self.driving_maneuver_client.call(direction="forward", steering="left", distance=2)
        # you can call the driving maneuver service like this
        # direction can be backward/forward, steering can be left/right/straight
        # self.driving_maneuver_client.call(direction="backward", steering="left", distance=0.3)
        elif request.direction == "left":
            self.driving_maneuver_client.call(direction="backward", steering="straight", distance=0.30)
            self.driving_maneuver_client.call(direction="backward", steering="left", distance=0.7)
            self.driving_maneuver_client.call(direction="backward", steering="straight", distance=1.2)
            self.driving_maneuver_client.call(direction="backward", steering="right", distance=0.52)
        elif request.direction == "right":
                self.driving_maneuver_client.call(direction="backward", steering="straight", distance=0.30)
                self.driving_maneuver_client.call(direction="backward", steering="right", distance=0.7)
                self.driving_maneuver_client.call(direction="backward", steering="straight", distance=1.2)
                self.driving_maneuver_client.call(direction="backward", steering="left", distance=0.52)
        else:
            return ParkingManeuverResponse(
                "ERROR: Request can only be 'left' or 'right' or 'straight'")

        return ParkingManeuverResponse("FINISHED")


if __name__ == "__main__":
    SimpleParkingManeuver()
    rospy.spin()
