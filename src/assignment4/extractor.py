#!/usr/bin/env python3

import rospy
import numpy as np
from sensor_msgs.msg import CameraInfo
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import cv2 as cv

# Hilfsfunktion für findCenter
def flattenList(list):
    flat_list = []
    for sublist in list:
        for item in sublist:
            flat_list.append(item)
    return flat_list

# Hilfsfunktion für Aufgabe 4-4
def findCenter(image):
    img = flattenList(cv.findNonZero(image))
    x = 0
    y = 0
    for pixel in img:
        x += pixel[0]
        y += pixel[1]
    return [x//len(img),y//len(img)]


def callback(data):
    # Aufgabe 4-2
    rospy.loginfo("---------------------")
    rospy.loginfo("Aufgabe 4-2")
    rospy.loginfo("fx = %s",data.K[0])
    rospy.loginfo("fy = %s",data.K[4])
    rospy.loginfo("cx = %s",data.K[2])
    rospy.loginfo("cy = %s",data.K[5])
    rospy.loginfo("distension paramters = %s",data.D)
    rospy.loginfo("---------------------")
    # Aufgabr 4-5
    cameraMatrix = np.array(data.K).reshape((3,3))
    realWorldPointArray = np.float32([[0.5,0.2,0],[0.5,-0.2,0],[0.8,0.2,0],[0.8,-0.2,0],[1.1,0.2,0],[1.1,-0.2,0]])
    whiteDotCenterArray = np.float32([[206, 38],[184, 24],[246, 12],[125, 10],[267,122],[96, 113]])
    ret, rvec, tvec = cv.solvePnP(realWorldPointArray,whiteDotCenterArray,cameraMatrix,data.D)
    rospy.loginfo("---------------------")
    rospy.loginfo("Aufgabe 4-5")
    rospy.loginfo("ret = %s",ret)
    rospy.loginfo("rvec = %s",rvec)
    rospy.loginfo("tvec = %s",tvec)
    rospy.loginfo("---------------------")
    # Aufgabe 4-6
    rospy.loginfo("---------------------")
    rospy.loginfo("Aufgabe 4-6")
    rospy.loginfo(cv.Rodrigues(rvec))
    rospy.loginfo("---------------------")

def callback2(data):
    # BILD Dimension 640 x 480
    # Aufgabe 4-3
    raw = bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
    greyscaled = raw
    cv.threshold(greyscaled,210,255,cv.THRESH_BINARY,greyscaled)
    edited = greyscaled
    # Schwarzer Balken oben
    edited = cv.rectangle(edited,(0,0),(640, 100),(0,0,0),-1)
    # Schwarzer Balken unten
    edited = cv.rectangle(edited,(0,350),(640, 480),(0,0,0),-1)
    # Schwarzer Balken mitte
    edited = cv.rectangle(edited,(260,240),(460, 480),(0,0,0),-1)
    # Schwarzer Balken links oben
    edited = cv.rectangle(edited,(0,100),(250,130),(0,0,0),-1)
    image_message_greyscaled = bridge.cv2_to_imgmsg(edited, encoding="passthrough")
    publisher.publish(image_message_greyscaled)
    # Aufgabe 4-4
    # Speichert Koordinaten nicht schwarzer Punkte
    locations = cv.findNonZero(edited)
    # Cropped images
    # top left [267,122]
    crop1 = edited[0:150,0:320]
    center1 = findCenter(crop1)
    # middle left [246, 12]
    crop2 = edited[150:200,0:320]
    center2 = findCenter(crop2)
    # bottom left [206, 38]
    crop3 = edited[200:480,0:320]
    center3 = findCenter(crop3)
    # top rigth [96, 113]
    crop4 = edited[0:140,320:640]
    center4 = findCenter(crop4)
    # middle rigth [125, 10]
    crop5 = edited[140:200,320:640]
    center5 = findCenter(crop5)
    # bottom rigth [184, 24]
    crop6 = edited[200:480,320:640]
    center6 = findCenter(crop6)
    rospy.loginfo("---------------------")
    rospy.loginfo("Aufgabe 4-4")
    rospy.loginfo("top left = %s",center1)
    rospy.loginfo("middle left = %s",center2)
    rospy.loginfo("bottom left = %s",center3)
    rospy.loginfo("top right = %s",center4)
    rospy.loginfo("middle right = %s",center5)
    rospy.loginfo("bottom right = %s",center6)
    rospy.loginfo("---------------------")

rospy.init_node('extractor')
bridge = CvBridge()
rospy.Subscriber('/sensors/camera/infra1/camera_info', CameraInfo, callback)
rospy.Subscriber('/sensors/camera/infra1/image_rect_raw', Image, callback2)
publisher = rospy.Publisher("/image_greyscaled", Image, queue_size=10)
rospy.spin()
