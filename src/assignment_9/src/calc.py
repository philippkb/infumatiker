#!/usr/bin/env python3

import matplotlib.pyplot as plt
import rospy
from autominy_msgs.msg import NormalizedSteeringCommand
from autominy_msgs.msg import SpeedCommand
from nav_msgs.msg import Odometry
from autominy_msgs.msg import Speed
from autominy_msgs.msg import SteeringFeedback
import math

lastOdometry = False
lastFeedback = False

def callback(data):
    global lastOdometry
    lastOdometry = data

def callback2(data):
    global lastFeedback
    lastFeedback = data

rospy.init_node('calculator')
publisher = rospy.Publisher("/actuators/steering_normalized", NormalizedSteeringCommand, queue_size=10)
publisher2 = rospy.Publisher("/actuators/speed", SpeedCommand, queue_size=10)
rospy.Subscriber('/simulation/odom_ground_truth', Odometry, callback)
rospy.Subscriber('/sensors/arduino/steering_angle', SteeringFeedback, callback2)

counter = 0
while not rospy.is_shutdown():
    counter += 1
    if counter == 2:
        rospy.loginfo("-----")
        rospy.loginfo("odom")
        rospy.loginfo(lastOdometry)
        rospy.loginfo("feed")
        rospy.loginfo(lastFeedback)
        rospy.loginfo("-----")
    if counter == 50:
        rospy.loginfo("-----")
        rospy.loginfo("odom")
        rospy.loginfo(lastOdometry)
        rospy.loginfo("feed")
        rospy.loginfo(lastFeedback)
        rospy.loginfo("-----")
    if counter >= 50:
        publisher.publish(value=0.0)
        publisher2.publish(value=0.0)
    else:
        publisher.publish(value=-1.0)
        publisher2.publish(value=0.3)
    rospy.sleep(0.1)

#rospy.spin()
