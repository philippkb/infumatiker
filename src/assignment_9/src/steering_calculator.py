#!/usr/bin/env python3

import matplotlib.pyplot as plt
import rospy
from autominy_msgs.msg import NormalizedSteeringCommand
from autominy_msgs.msg import SpeedCommand
from nav_msgs.msg import Odometry
from autominy_msgs.msg import Speed
import math

last_x, last_y, last_secs  = 0,0,0
speedArray = []
timeArray = []

def calculateVelocity(time1,x1,y1,time2,x2,y2):
    return math.sqrt((x1-x2)**2 + (y1-y2)**2)/math.sqrt((time1-time2)**2)


def callback(data):
    global speedArray, last_x, last_y, timeArray, last_secs
    current_x = data.pose.pose.position.x
    current_y = data.pose.pose.position.y
    current_secs = data.header.stamp.secs + (data.header.stamp.nsecs / 1000000000.0)
    velocity = calculateVelocity(current_secs,current_x,current_y,last_secs,last_x,last_y)
    speedArray = speedArray + [velocity]
    timeArray = timeArray + [current_secs]
    last_x = current_x
    last_y = current_y
    last_secs = current_secs
    if(current_secs - timeArray[0] < 4):
        publisher.publish(value=0.0)
        publisher2.publish(value=0.3)
    else:
        publisher2.publish(value=0.0)
        plt.plot(timeArray[1:],speedArray[1:])
        plt.show()


rospy.init_node('calculator')
publisher = rospy.Publisher("/actuators/steering_normalized", NormalizedSteeringCommand, queue_size=10)
publisher2 = rospy.Publisher("/actuators/speed", SpeedCommand, queue_size=10)
rospy.Subscriber('/simulation/odom_ground_truth', Odometry, callback)
rospy.spin()
