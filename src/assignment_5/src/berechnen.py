#!/usr/bin/env python3
import matplotlib.pyplot as plt
from autominy_msgs.msg import Tick
from nav_msgs.msg import Odometry
from autominy_msgs.msg import Speed
from collections import deque
import rospy


# Hilfsfunktion für 5-3 (b)
def calculateSpeed(x):
    return  (0.2851*x)

# Aufgabe 5-3 (a)
# Addiert alle Ticks auf
tickcounter = 0
ticks = 0

def callbackTick(data):
    global tickcounter
    tickcounter = tickcounter + int(data.value)
    rospy.loginfo(tickcounter)

# Aufgabe 5-3 (b)
# Berechnet den durchschnittlichen Wert pro Tick (der nicht 0 ist)
def callbackTick2(data):
    global tickcounter, ticks
    if (int(data.value) > 0):
        ticks = ticks + 1.0
    rospy.loginfo(tickcounter)
    rospy.loginfo("Average Values per Tick:" )
    rospy.loginfo(float(tickcounter)/float(ticks))

# Aufgabe 5-3 (b)
# Errechnet Geschwindigkeit von Fahrzeug
# Und plottet nach 300 Ticks (die nicht 0 sind, also 3 Sekunden) den Graphen
ringbuffer = deque([],50)
yValues = []
for i in range(0,301):
    yValues.append(i/100)
plotArray = [0]
plotFlag = True

def callbackTick3(data):
    global ringbuffer, plotArray, plotFlag, yValues
    ringbuffer.append(int(data.value))
    ticks = 0
    for elem in ringbuffer:
        ticks += elem
    speed = calculateSpeed(ticks/len(ringbuffer))
    publisher.publish(value=speed)

    if (int(data.value)>0 and len(plotArray)<301):
        plotArray.append(speed)
    if (len(plotArray)>=301 and plotFlag):
        plotFlag = False
        plt.plot(yValues, plotArray)
        plt.show()

rospy.init_node('berechnen')
rospy.Subscriber('/sensors/arduino/ticks', Tick, callbackTick3)
publisher = rospy.Publisher("/autominy_msgs/speed", Speed, queue_size=10)
rospy.spin()
