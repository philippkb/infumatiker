#!/usr/bin/env python3

import rospy
from autominy_msgs.msg import NormalizedSteeringCommand
from autominy_msgs.msg import SpeedCommand

publisher = rospy.Publisher("/actuators/steering_normalized", NormalizedSteeringCommand, queue_size=10)
publisher2 = rospy.Publisher("/actuators/speed", SpeedCommand, queue_size=10)

rospy.init_node('publish_steering_command')

while not rospy.is_shutdown():
    publisher.publish(value=1.0)
    publisher2.publish(value=0.3)
    rospy.sleep(0.5)
