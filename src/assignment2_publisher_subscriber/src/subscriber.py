#!/usr/bin/env python3

import rospy
from autominy_msgs.msg import Speed

def callback(data):
    rospy.loginfo("I heard %s",data)

rospy.Subscriber('/sensors/speed', Speed, callback)
rospy.init_node('speed_subscriber')
rospy.spin()
