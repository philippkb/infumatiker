#!/usr/bin/env python3

import matplotlib.pyplot as plt
import rospy
from autominy_msgs.msg import SteeringCommand
from autominy_msgs.msg import SpeedCommand
from nav_msgs.msg import Odometry
from autominy_msgs.msg import Speed
import math

last_x = 0
last_y = 0
last_secs = 0
last_error = 0

current_x = 0
current_y = 0
current_secs = 0
current_error = 0

error_array = []

def calculateRadiant(x1,y1,x2,y2):
    x_new = x1-x2
    y_new = y1-y2
    vektor_length = math.sqrt(x_new**2 + y_new**2)
    winkel = math.acos(x_new/vektor_length)
    if (y_new > 0):
        return 1*winkel
    else: return -1*winkel

def calculateLeftOrRight(current_radiant, target_radiant):
    distance_one = (current_radiant-target_radiant)
    abs_distance_one = abs(distance_one)
    distance_two = 2*math.pi - abs_distance_one
    if abs_distance_one < distance_two:
        if distance_one > 0:
            return -abs_distance_one
        else: return abs_distance_one
    else:
        if distance_one > 0:
            return distance_two
        else: return -distance_two

def callback(data):
        global current_x, current_y, current_secs, last_x, last_y, last_secs
        last_x = current_x
        last_y = current_y
        last_secs = current_secs

        current_x = data.pose.pose.position.x
        current_y = data.pose.pose.position.y
        current_secs = data.header.stamp.secs + (data.header.stamp.nsecs / 1000000000.0)

        controller(SteeringCommand(value=math.pi))

def controller(SteeringCommand):
    global current_x, current_y, current_secs, current_error, last_x, last_y, last_secs, last_error

    target_radiant = SteeringCommand.value

    Kp = 1.5
    Ki = 0.0005
    Kd= 0.3

    current_radiant = calculateRadiant(current_x,current_y,last_x,last_y)
    last_error = current_error
    current_error = calculateLeftOrRight(current_radiant,target_radiant)

    error_change = (current_error-last_error)/(current_secs-last_secs)

    controller_result =  (Kp*(current_error) + Kd*(-error_change) + Ki*sum(error_array))
    if controller_result > math.pi: result = math.pi
    if controller_result < -math.pi: result = -math.pi

    publisher.publish(value=0.5)
    publisher2.publish(value=controller_result)


rospy.init_node('steering_controller')
rospy.Subscriber('/simulation/odom_ground_truth', Odometry, callback)
publisher = rospy.Publisher("/actuators/speed", SpeedCommand, queue_size=10)
publisher2 = rospy.Publisher("/actuators/steering", SteeringCommand, queue_size=10)
rospy.spin()
