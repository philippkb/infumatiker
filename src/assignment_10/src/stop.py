#!/usr/bin/env python3
from autominy_msgs.msg import SpeedCommand
import rospy

rospy.init_node('stop')
publisher = rospy.Publisher("/actuators/speed", SpeedCommand, queue_size=10)
publisher.publish(value=0.0)
rospy.sleep(0.5)
publisher.publish(value=0.0)
rospy.sleep(0.5)
publisher.publish(value=0.0)
rospy.sleep(0.5)
