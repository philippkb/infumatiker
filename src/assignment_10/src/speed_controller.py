#!/usr/bin/env python3

import matplotlib.pyplot as plt
import rospy
from autominy_msgs.msg import NormalizedSteeringCommand
from autominy_msgs.msg import SpeedCommand
from nav_msgs.msg import Odometry
from autominy_msgs.msg import Speed
import math

wanted_speed = 0.0

last_x = 0
last_y = 0
last_secs = 0
last_error = 0

current_x = 0
current_y = 0
current_secs = 0
current_error = 0

error_array = []

def calculateVelocity(time1,x1,y1,time2,x2,y2):
    return math.sqrt((x1-x2)**2 + (y1-y2)**2)/math.sqrt((time1-time2)**2)

def callback(data):
        global current_x, current_y, current_secs, last_x, last_y, last_secs
        last_x = current_x
        last_y = current_y
        last_secs = current_secs

        current_x = data.pose.pose.position.x
        current_y = data.pose.pose.position.y
        current_secs = data.header.stamp.secs + (data.header.stamp.nsecs / 1000000000.0)

        controller(SpeedCommand(value=wanted_speed))


def controller(SpeedCommand):
    global current_x, current_y, current_secs, current_error, last_x, last_y, last_secs, last_error, error_array, controllerOutputArray, wantedSpeedArray, currentSpeedArray, timeArray

    desired_speed = SpeedCommand.value

    Kp = 0.8
    Ki = 0.05
    Kd = 0.3

    current_velocity = calculateVelocity(current_secs,current_x,current_y,last_secs,last_x,last_y)
    last_error = current_error
    current_error = desired_speed-current_velocity
    error_change = (current_error-last_error)/(current_secs-last_secs)
    if error_change > desired_speed: error_change = desired_speed
    if error_change < -desired_speed: error_change = -desired_speed

    controller_result =  (Kp*(current_error) + Kd*(-error_change) + Ki*sum(error_array))
    if controller_result<0: result = 0

    a = str(Kp*(current_error))
    b = str(Kd*(-error_change))
    c = str(Ki*sum(error_array))
    rospy.loginfo(a + "  " + b + "  " + c)

    error_array += [current_error]

    publisher.publish(value=controller_result)
    publisher2.publish(value=0.1)


rospy.init_node('speed_controller')
rospy.Subscriber('/simulation/odom_ground_truth', Odometry, callback)
publisher = rospy.Publisher("/actuators/speed", SpeedCommand, queue_size=10)
publisher2 = rospy.Publisher("/actuators/steering_normalized", NormalizedSteeringCommand, queue_size=10)
rospy.spin()
