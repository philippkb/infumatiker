#!/usr/bin/env python3
import rospy
from autominy_msgs.msg import SpeedCommand, NormalizedSteeringCommand
from sensor_msgs.msg import LaserScan


# Notiz für die Abgabe
# Die Verwirrung kommt wahrscheinlich durch das Wählen vom Startwert
# Hier wurde der mit -Pi initialisiert. Dadurch gibt es auch negative Werte
# Der Initialwert kann aber eig willkürlich gewählt werden
# Wichtig ist, dass die Scandaten von vor dem Auto am Anfang und am Ende des Arrays stehen
# Wäre mit 0 initialisiert, dann würde es von 0 bis 2pi gehen und hinter dem Auto wäre dementsprechend auch pi




def callback(data):
    global speed, steering_angle
    # Splitt into different regions
    publisher_speed.publish(value=speed)
    publisher_steering_angle.publish(value=steering_angle)
    frontArea = []
    leftArea = []
    rightArea = []
    angle = data.angle_min
    incrementValue = data.angle_increment
    #rospy.loginfo(data.ranges)
    for distance in data.ranges:
        # Falls etwas beim Inkrementieren schief gelaufen ist
        if (angle > data.angle_max):
            break
        # Trägt nur ein wenn die Daten valide sind
        if (distance >= data.range_min and distance <= data.range_max):
            # Angles for the front Area
            if (3.0 < angle < 3.1 or -3.1 < angle < -3.0 ):
                if (distance > 0.2): frontArea += [distance]
            if ( 2.1 < angle < 2.6):
                rightArea += [distance]
            if ( -2.6 < angle < -2.1):
                leftArea += [distance]
        angle += incrementValue

    if (any(x <=0.7 for x in frontArea)):
        rospy.loginfo("COLLISION FRONT!!!")
        steering_angle = 1.0
        if (any(x <=0.3 for x in leftArea)):
            rospy.loginfo("COLLISION LEFT!!!")
            steering_angle = -1.0
            if (any(x <=0.3 for x in rightArea)):
                rospy.loginfo("COLLISION RIGHT!!!")
                speed = 0

    else:
        rospy.loginfo('No COLLISION')
        steering_angle = 0

speed = 0.3
steering_angle = 0
rospy.init_node("Obstacle_Avoidance")
publisher_steering_angle = rospy.Publisher("/actuators/steering_normalized", NormalizedSteeringCommand, queue_size=10)
publisher_speed = rospy.Publisher("/actuators/speed", SpeedCommand, queue_size=10)
rospy.Subscriber('/sensors/rplidar/scan', LaserScan, callback)
rospy.spin()
