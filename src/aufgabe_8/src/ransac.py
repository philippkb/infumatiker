#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import cv2 as cv
import random
import numpy as np
from numpy.linalg import norm
import copy

def flattenList(list):
    flat_list = []
    for sublist in list:
        for item in sublist:
            flat_list.append(item)
    return flat_list

# Für einen Performanceboost gucken wir in etwas größeren Schritten
def check_even(p):
    return p[0] % 4 == 0

def callback(data):
    raw = bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
    imageCopy = copy.deepcopy(raw)
    # Deckt den Horizont ab, da der nicht interessant ist aber weiß enthält
    raw = cv.rectangle(raw,(0,0),(800,120),[0,0,0],-1)
    cv.threshold(raw,210,255,cv.THRESH_BINARY,raw)
    image_message_greyscaled = bridge.cv2_to_imgmsg(raw, encoding="passthrough")
    publisher2.publish(image_message_greyscaled)
    whitePixel = list(filter(check_even,flattenList(cv.findNonZero(raw))))
    # ransac für die erste Linie
    [inliers, outliers] = ransacSimple(whitePixel)
    p1 = (inliers[0][0],inliers[0][1])
    p2 = (inliers[-1][0],inliers[-1][1])
    imageCopy = cv.line(imageCopy,p1,p2,(0,0,0),10)
    # ransac für die zweite Linie
    [inliers, outliers] = ransacSimple(outliers)
    p1 = (inliers[0][0],inliers[0][1])
    p2 = (inliers[-1][0],inliers[-1][1])
    imageCopy = cv.line(imageCopy,p1,p2,(0,0,0),10)
    image_message_marked = bridge.cv2_to_imgmsg(imageCopy, encoding="passthrough")
    publisher.publish(image_message_marked)

# bekommt eine Liste mit allen weißen Punkten
def ransacSimple(data):
    bestInliers = [[],[]]
    for i in range(0,20):
        p1 = random.randint(0,len(data)-1)
        p2 = random.randint(0,len(data)-1)
        while(p1 == p2):
            p2 = random.randint(0,len(data)-1)
        inliers = findInliers(data[p1],data[p2],data)
        if len(inliers[0]) > len(bestInliers[0]):
            bestInliers = inliers
    return bestInliers

def findInliers(p1,p2,data):
    threshold = 20
    distances = np.cross(p2-p1,data-p1)/norm(p2-p1)
    inliers = []
    outliers = []
    for i in range(0,len(distances)):
        if -threshold <= distances[i] <= threshold:
            inliers = inliers + [data[i]]
        else:
            outliers = outliers + [data[i]]
    return [inliers,outliers]

rospy.init_node('extractor')
bridge = CvBridge()
rospy.Subscriber('/sensors/camera/infra1/image_rect_raw', Image, callback)
publisher = rospy.Publisher("/image_lane_recognition", Image, queue_size=10)
publisher2 = rospy.Publisher("/image_lane_segmentation", Image, queue_size=10)
rospy.spin()
