#!/usr/bin/env python3

import rospy
import math
import numpy as np
import tf2_ros as tf2
from sensor_msgs.msg import LaserScan, PointCloud2
from sensor_msgs import point_cloud2
from geometry_msgs.msg import Point
from std_msgs.msg import Header
from tf2_sensor_msgs.tf2_sensor_msgs import do_transform_cloud
from nav_msgs.msg import OccupancyGrid

def polarToCarth(distance,angle):
    x = distance * math.cos(angle)
    y = distance * math.sin(angle)
    return  [x,y,0]


def callback(data):
    global tf2Buffer, grid_msg
    # Aufgabe 6-2 a)
    # Define Header
    header = Header()
    header.stamp = rospy.Time.now()
    header.frame_id = "laser"
    # Define Points Array
    transformedPointArray = []
    # Startet beim Startwinkel und erhöht nach jedem Eintrag um angle_increment
    angle = data.angle_min
    incrementValue = data.angle_increment
    for distance in data.ranges:
        # Falls etwas beim Inkrementieren schief gelaufen ist
        if (angle > data.angle_max):
            break
        # Trägt nur ein wenn die Daten valide sind
        if (distance >= data.range_min and distance <= data.range_max):
            transformedPointArray.append(polarToCarth(distance, angle))
        angle += incrementValue
    # Create point_cloud2
    pointCloud = point_cloud2.create_cloud_xyz32(header, points=transformedPointArray)
    # Publish original PointCloud2 in laser Frame
    #publisher.publish(pointCloud)

    # Aufgabe 6-2 b)
    # Transform Frames
    trans = tf2Buffer.lookup_transform("lab", "laser", rospy.Time(0), rospy.Duration(0.7))
    #rospy.loginfo(trans)
    newPointCloud = do_transform_cloud(pointCloud,trans)
    # Publish
    publisher.publish(newPointCloud)

    # Aufgabe 6-2 c)
    # To OccupancyGrid
    gen = point_cloud2.read_points(pointCloud)
    # Into 2d Array
    newGrid = np.reshape(grid_msg.data,(800,900))
    # Store Points from newPointCloud into grid_msg
    for p in gen:
        x = int(round(p[0],2)*100)
        y = int(round(p[1],2)*100)
        newGrid[y][x] = 100
    # Into List
    grid_msg.data = newGrid.astype(np.int8).flatten().tolist()
    #Publish Grid
    publisher2.publish(grid_msg)

rospy.init_node("LiDar")
tf2Buffer = tf2.Buffer()
listener = tf2.TransformListener(tf2Buffer)
# Create Occupancy Grid
grid_msg = OccupancyGrid()
grid_msg.header.frame_id = 'lab'
grid_msg.header.stamp = rospy.Time.now()
grid_msg.info.resolution = 0.01
grid_msg.info.width = 900
grid_msg.info.height = 800
grid_msg.data = np.zeros((800, 900), dtype=np.int8).astype(np.int8).flatten().tolist()
# grid_msg.info.pose fehlt daher ist die Orientierung nicht mittig
rospy.Subscriber('/sensors/rplidar/scan', LaserScan, callback)
publisher = rospy.Publisher('/sensor_msgs/PointCloud2', PointCloud2, queue_size=10)
publisher2 = rospy.Publisher('nav_msgs/OccupancyGrid', OccupancyGrid, queue_size=10)
rospy.spin()
