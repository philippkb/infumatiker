#!/usr/bin/env python3

import rospy
from autominy_msgs.msg import NormalizedSteeringCommand
from autominy_msgs.msg import SpeedCommand

publisher = rospy.Publisher("/actuators/steering_normalized", NormalizedSteeringCommand, queue_size=10)
publisher2 = rospy.Publisher("/actuators/speed", SpeedCommand, queue_size=10)
rospy.Subscriber('/simulation/odom ground truth', Speed, callback)

rospy.init_node('calculator')

publisher.publish(value=1.0)
publisher2.publish(value=0.3)
rospy.sleep(0.5)
publisher.publish(value=1.0)
publisher2.publish(value=0.3)
rospy.sleep(0.5)
publisher.publish(value=1.0)
publisher2.publish(value=0.3)
rospy.sleep(0.5)
publisher.publish(value=1.0)
publisher2.publish(value=0.3)
rospy.sleep(0.5)
while not rospy.is_shutdown():
    publisher2.publish(value=0.0)

#rospy.spin()
#rospy.sleep(2)
#publisher2.publish(value=0.0)
